import 'dart:io';

class Taobin {
  int price = 0;

  display() {
    print('Welcome to Taobin');
    print('Please select a Categories.');
  }

  showCategories() {
    print('1. Coffee');
    print('2. Tea');
    print('3. Milk');
    print('4. Soda');
  }

  showMenu(int choice) {
    switch (choice) {
      case 1:
        Coffee coffee = Coffee();
        print('Select your menu in Coffee Categories');
        //show menu name in coffee categories and ask user to choose menu name
        coffee.listCoffee();
        int chooseCoffee = int.parse(stdin.readLineSync()!);
        coffee.setMenu(chooseCoffee);
        showType(); //show type of menu (Hot,Cool or Smoothie)
        int chooseType = int.parse(stdin.readLineSync()!);
        coffee.setType(chooseType);
        sweetLevel(); //show Sweet Level of menu 
        int chooseSweet = int.parse(stdin.readLineSync()!);
        coffee.setSweetLevel(chooseSweet);
        addTopping(); //show list of Topping 
        int chooseTopping = int.parse(stdin.readLineSync()!);
        coffee.setTopping(chooseTopping);
        takeStraw(); //ask user for take straw (Y/N)
        int chooseStraw = int.parse(stdin.readLineSync()!);
        coffee.setStraw(chooseStraw);
        takeLid(); //ask user for take Lid (Y/N)
        int chooseLid = int.parse(stdin.readLineSync()!);
        coffee.setLid(chooseLid);
        coffee.showOrder(); //show all detail of order
        price = coffee.getPrice();
        print('Your total price is $price');
        break;

      case 2:
        Tea tea = Tea();
        print('Select your menu in Tea Categories');
        tea.listTea(); //show menu name in Tea categories and ask user to choose menu name
        int chooseTea = int.parse(stdin.readLineSync()!);
        tea.setMenu(chooseTea);
        showType(); //show type of menu (Hot,Cool or Smoothie)
        int chooseType = int.parse(stdin.readLineSync()!);
        tea.setType(chooseType);
        sweetLevel(); //show Sweet Level of menu 
        int chooseSweet = int.parse(stdin.readLineSync()!);
        tea.setSweetLevel(chooseSweet);
        addTopping(); //show list of Topping 
        int chooseTopping = int.parse(stdin.readLineSync()!);
        tea.setTopping(chooseTopping);
        takeStraw(); //ask user for take straw (Y/N)
        int chooseStraw = int.parse(stdin.readLineSync()!);
        tea.setStraw(chooseStraw);
        takeLid(); //ask user for take Lid (Y/N)
        int chooseLid = int.parse(stdin.readLineSync()!);
        tea.setLid(chooseLid);
        tea.showOrder(); //show all detail of order
        price = tea.getPrice();
        print('Your total price is $price');
        break;

      case 3:
        Milk milk = Milk();
        print('Select your menu in Milk Categories');
        milk.listMilk(); //show menu name in Milk categories and ask user to choose menu name
        int chooseMilk = int.parse(stdin.readLineSync()!);
        milk.setMenu(chooseMilk);
        showType(); //show type of menu (Hot,Cool or Smoothie)
        int chooseType = int.parse(stdin.readLineSync()!);
        milk.setType(chooseType);
        sweetLevel(); //show Sweet Level of menu 
        int chooseSweet = int.parse(stdin.readLineSync()!);
        milk.setSweetLevel(chooseSweet);
        addTopping(); //show list of Topping 
        int chooseTopping = int.parse(stdin.readLineSync()!);
        milk.setTopping(chooseTopping);
        takeStraw(); //ask user for take straw (Y/N)
        int chooseStraw = int.parse(stdin.readLineSync()!);
        milk.setStraw(chooseStraw);
        takeLid();//ask user for take Lid (Y/N)
        int chooseLid = int.parse(stdin.readLineSync()!);
        milk.setLid(chooseLid);
        milk.showOrder();//show all detail of order
        price = milk.getPrice();
        print('Your total price is $price');
        break;

      case 4:
        Soda soda = Soda();
        print('Select your menu in Soda Categories');
        soda.listSoda(); //show menu name in Milk categories and ask user to choose menu name
        int chooseSoda = int.parse(stdin.readLineSync()!);
        soda.setMenu(chooseSoda);
        takeStraw(); //ask user for take straw (Y/N)
        int chooseStraw = int.parse(stdin.readLineSync()!);
        soda.setStraw(chooseStraw);
        takeLid(); //ask user for take Lid (Y/N)
        int chooseLid = int.parse(stdin.readLineSync()!);
        soda.setLid(chooseLid);
        soda.showOrder(); //show all detail of order
        price = soda.getPrice();
        print('Your total price is $price');
        break;
      default:
    }
  }

  getMoney(){
    print("Please input your money");
    int money = int.parse(stdin.readLineSync()!);
    price = price - money;
    int remain = price;
    if( remain > 0 ){
      int remain = price;
      print('Your input money : $money');
      print('Remaining : $remain');
      getMoney();
    }
    else if ( remain < 0 ) {
      int exchange = remain - remain - remain;
      print('Exchange : $exchange');
      print('Please wait for your order,Thank you');
    }
    else {
      print("Please wait for your order,Thank you");
    }
  }
}

showType() {
  print('Please Choose menu type');
  print('1. Hot');
  print('2. Cool (+5฿)');
  print('3. Smoothie (+10฿)');
}

sweetLevel() {
  print("Please choose your sweet level.");
  print('1. No Sugar');
  print('2. Less Sweet');
  print('3. Just right');
  print('4. Sweet');
  print('5. Very Sweet');
}

addTopping() {
  print('Please choose your Extra Topping');
  print('1. 1 Shot of Espresso (+฿15)');
  print('2. Brown Sugar kojac boba (+฿10)');
  print('3. No Topping');
}

takeStraw() {
  print('Do you need a straw ?');
  print('1. Yes');
  print('2. No');
}

takeLid() {
  print('Do you need a Lid ?');
  print('1. Yes');
  print('2. No');
}

class Coffee {
  String menu = ''; //for show menu that user pick
  String type = ''; //type of user's menu : Hot,Cool, Smoothie
  String sweetLevel = ''; //for show sweet level
  String addTopping = ''; //1.add shot 2.add brown sugar konjac boba 3.none
  String straw = ''; //ask user yes,no
  String lid = ''; //ask user yes,no
  int price = 40;

  listCoffee() {
    List listCoffee = [
      '1. Espresso',
      '2. Cappuccino',
      '3. Latte',
      '4. Mocha',
      '5. Americano'
    ];
    for (var coffee in listCoffee) {
      print(coffee);
    }
  }

  setMenu(int menuName) {
    switch (menuName) {
      case 1:
        menu = 'Espresso';
        break;
      case 2:
        menu = 'Cappuccino';
        break;
      case 3:
        menu = 'Latte';
        break;
      case 4:
        menu = 'Mocha';
        break;
      case 5:
        menu = 'Americano';
        break;
      default:
    }
  }

  setSweetLevel(int level) {
    switch (level) {
      case 1:
        sweetLevel = 'No Sugar';
        break;
      case 2:
        sweetLevel = 'Less Sweet';
        break;
      case 3:
        sweetLevel = 'Just right';
        break;
      case 4:
        sweetLevel = 'Sweet';
        break;
      case 5:
        sweetLevel = 'Very Sweet';
        break;
      default:
    }
  }

  setTopping(int topping) {
    switch (topping) {
      case 1:
        addTopping = '1 Shot of Espresso (+฿15)';
        price = price + 15;
        break;
      case 2:
        addTopping = 'Brown Sugar kojac boba (+฿10)';
        price = price + 10;
        break;
      case 3:
        addTopping = 'No Topping';
        break;
      default:
    }
  }

  setStraw(int getstraw) {
    switch (getstraw) {
      case 1:
        straw = 'Yes';
        break;
      case 2:
        straw = 'No';
        break;
      default:
    }
  }

  setLid(int getLid) {
    switch (getLid) {
      case 1:
        lid = 'Yes';
        break;
      case 2:
        lid = 'No';
        break;
      default:
    }
  }

  setType(int typeName) {
    switch (typeName) {
      case 1:
        type = 'Hot';

        break;
      case 2:
        type = 'Cool (+5฿)';
        price = price + 5;

        break;
      case 3:
        type = 'Smoothie (+10฿)';
        price = price + 10;

        break;
    }
  }

  getPrice() {
    return price;
  }

  showOrder() {
    print('This is your order');
    print('Your Coffee menu : $menu ');
    print('Type : $type');
    print('Sweet Level : $sweetLevel');
    print('Add Topping : $addTopping');
    print('Take Straw : $straw');
    print('Take Lid : $lid');
  }
}

class Tea {
  String menu = ''; //for show menu that user pick
  String type = ''; //type of user's menu : Hot,Cool, Smoothie
  String sweetLevel = ''; //for show sweet level
  String addTopping = ''; //1.add shot 2.add brown sugar konjac boba 3.none
  String straw = ''; //ask user yes,no
  String lid = ''; //ask user yes,no
  int price = 40;

  listTea() {
    List listTea = [
      '1. Thai Milk Tea',
      '2. Taiwanese Tea',
      '3. Matcha Latte',
      '4. Kokuto Tea',
      '5. Lime Tea'
    ];

    for (var tea in listTea) {
      print(tea);
    }
  }

  setMenu(int menuName) {
    switch (menuName) {
      case 1:
        menu = 'Espresso';
        break;
      case 2:
        menu = 'Cappuccino';
        break;
      case 3:
        menu = 'Latte';
        break;
      case 4:
        menu = 'Mocha';
        break;
      case 5:
        menu = 'Americano';
        break;
      default:
    }
  }

  setSweetLevel(int level) {
    switch (level) {
      case 1:
        sweetLevel = 'No Sugar';
        break;
      case 2:
        sweetLevel = 'Less Sweet';
        break;
      case 3:
        sweetLevel = 'Just right';
        break;
      case 4:
        sweetLevel = 'Sweet';
        break;
      case 5:
        sweetLevel = 'Very Sweet';
        break;
      default:
    }
  }

  setTopping(int topping) {
    switch (topping) {
      case 1:
        addTopping = '1 Shot of Espresso (+฿15)';
        price = price + 15;
        break;
      case 2:
        addTopping = 'Brown Sugar kojac boba (+฿10)';
        price = price + 10;
        break;
      case 3:
        addTopping = 'No Topping';
        break;
      default:
    }
  }

  setStraw(int getstraw) {
    switch (getstraw) {
      case 1:
        straw = 'Yes';
        break;
      case 2:
        straw = 'No';
        break;
      default:
    }
  }

  setLid(int getLid) {
    switch (getLid) {
      case 1:
        lid = 'Yes';
        break;
      case 2:
        lid = 'No';
        break;
      default:
    }
  }

  setType(int typeName) {
    switch (typeName) {
      case 1:
        type = 'Hot';

        break;
      case 2:
        type = 'Cool (+5฿)';
        price = price + 5;

        break;
      case 3:
        type = 'Smoothie (+10฿)';
        price = price + 10;

        break;
    }
  }

  getPrice() {
    return price;
  }

  showOrder() {
    print('This is your order');
    print('Your Coffee menu : $menu ');
    print('Type : $type');
    print('Sweet Level : $sweetLevel');
    print('Add Topping : $addTopping');
    print('Take Straw : $straw');
    print('Take Lid : $lid');
  }
}

class Milk {
  String menu = ''; //for show menu that user pick
  String type = ''; //type of user's menu : Hot,Cool, Smoothie
  String sweetLevel = ''; //for show sweet level
  String addTopping = ''; //1.add shot 2.add brown sugar konjac boba 3.none
  String straw = ''; //ask user yes,no
  String lid = ''; //ask user yes,no
  int price = 40;

  listMilk() {
    List listMilk = [
      '1. Caramel Milk',
      '2. Caramel Cocoa',
      '3. Milk',
      '4. Pink Milk',
      '5. Cocoa'
    ];
    for (var milk in listMilk) {
      print(milk);
    }
  }

  setMenu(int menuName) {
    switch (menuName) {
      case 1:
        menu = 'Espresso';
        break;
      case 2:
        menu = 'Cappuccino';
        break;
      case 3:
        menu = 'Latte';
        break;
      case 4:
        menu = 'Mocha';
        break;
      case 5:
        menu = 'Americano';
        break;
      default:
    }
  }

  setSweetLevel(int level) {
    switch (level) {
      case 1:
        sweetLevel = 'No Sugar';
        break;
      case 2:
        sweetLevel = 'Less Sweet';
        break;
      case 3:
        sweetLevel = 'Just right';
        break;
      case 4:
        sweetLevel = 'Sweet';
        break;
      case 5:
        sweetLevel = 'Very Sweet';
        break;
      default:
    }
  }

  setTopping(int topping) {
    switch (topping) {
      case 1:
        addTopping = '1 Shot of Espresso (+฿15)';
        price = price + 15;
        break;
      case 2:
        addTopping = 'Brown Sugar kojac boba (+฿10)';
        price = price + 10;
        break;
      case 3:
        addTopping = 'No Topping';
        break;
      default:
    }
  }

  setStraw(int getstraw) {
    switch (getstraw) {
      case 1:
        straw = 'Yes';
        break;
      case 2:
        straw = 'No';
        break;
      default:
    }
  }

  setLid(int getLid) {
    switch (getLid) {
      case 1:
        lid = 'Yes';
        break;
      case 2:
        lid = 'No';
        break;
      default:
    }
  }

  setType(int typeName) {
    switch (typeName) {
      case 1:
        type = 'Hot';

        break;
      case 2:
        type = 'Cool (+5฿)';
        price = price + 5;

        break;
      case 3:
        type = 'Smoothie (+10฿)';
        price = price + 10;

        break;
    }
  }

  getPrice() {
    return price;
  }

  showOrder() {
    print('This is your order');
    print('Your Coffee menu : $menu ');
    print('Type : $type');
    print('Sweet Level : $sweetLevel');
    print('Add Topping : $addTopping');
    print('Take Straw : $straw');
    print('Take Lid : $lid');
  }
}

class Soda {
  String menu = ''; //for show menu that user pick
  String type = ''; //type of user's menu : Hot,Cool, Smoothie
  String sweetLevel = ''; //for show sweet level
  String addTopping = ''; //1.add shot 2.add brown sugar konjac boba 3.none
  String straw = ''; //ask user yes,no
  String lid = ''; //ask user yes,no
  int price = 45;

  listSoda() {
    List listSoda = [
      '1. Pepsi',
      '2. Limeade Soda',
      '3. Lychee Soda',
      '4. Strawberry Soda',
      '5. Blueberry Soda'
    ];

    for (var soda in listSoda) {
      print(soda);
    }
  }

  setMenu(int menuName) {
    switch (menuName) {
      case 1:
        menu = 'Pepsi';
        break;
      case 2:
        menu = 'Limeade Soda';
        break;
      case 3:
        menu = 'Lychee Soda';
        break;
      case 4:
        menu = 'Strawberry Soda';
        break;
      case 5:
        menu = 'Blueberry Soda';
        break;
      default:
    }
  }

  setStraw(int getstraw) {
    switch (getstraw) {
      case 1:
        straw = 'Yes';
        break;
      case 2:
        straw = 'No';
        break;
      default:
    }
  }

  setLid(int getLid) {
    switch (getLid) {
      case 1:
        lid = 'Yes';
        break;
      case 2:
        lid = 'No';
        break;
      default:
    }
  }

  getPrice() {
    return price;
  }

  showOrder() {
    print('This is your order');
    print('Your Coffee menu : $menu ');
    print('Take Straw : $straw');
    print('Take Lid : $lid');
  }
}

void main() {
  Taobin order = Taobin();
  order.display();
  order.showCategories();
  int selectCategories = int.parse(stdin.readLineSync()!);
  order.showMenu(selectCategories);
  order.getMoney();
}
